var testImage *ebiten.Image

func init() {
	img, _, err := ebitenutil.NewImageFromFile("client/assets/test.png")
	if err != nil {
		log.Fatalf("reading image: %v", err)
	}
	testImage = img
}

func (g *Game) Draw(screen *ebiten.Image) {
	screen.DrawImage(testImage, &ebiten.DrawImageOptions{})
}