package main

import (
	"log"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

// Game implements ebiten.Game interface.
type Game struct{}

func (g *Game) Update() error {
	// Game's update logic comes here.
	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	// Game's drawing logic comes here.
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return 640, 480 // Set the default screen size.
}

func main() {
	ebiten.SetWindowSize(640, 480)
	ebiten.SetWindowTitle("Snake Online")
	if err := ebiten.RunGame(&Game{}); err != nil {
		log.Fatal(err)
	}
}

func init() {
	img, _, err := ebitenutil.NewImageFromFile("client/assets/test.png")
	if err != nil {
		log.Fatalf("reading image: %v", err)
	}
	testImage = img
}
